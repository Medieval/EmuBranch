
First, Rename EmuConfig.txt to EmuConfig.xml  - OR - download the file from https://drive.google.com/open?id=14VxZx6uHZgY_FlybvAAeCdsIkVVYhAmV

Copy the EmuConfig.xml to the Root folder of the Game installation.

My folder looks like this for example:

C:\7D2D\MedievalMod\Medieval_Mod_-_Book_1_-_EmuBranch

This is where I would copy the folder.  You will see the file 7DaysToDie.exe in this same folder.



To change the Language, use Notepad or any other program to edit this value:

value="Japanese" <---- to your preferred Language that is supported

  Currently supported languages:
    Japanese
    English
    French
    German
    Spanish
