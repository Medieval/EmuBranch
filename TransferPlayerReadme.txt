Instructions to transfer or copy your Player folder to a brand new world map:

This only works for single player or local hosted games.  Does not work on a dedicated server.

This is for Windows 7, 8, 10:

Login to your current game and put all the items you want to take with you to the new map, into your backpack.
You will only transfer with whatever is in your backpack, so choose wisely!

Important:  The map was made smaller to 8k so move your player somewhere closer to the center of map
so that when you login, you won't be in a radiated zone if you are farther than 8k from the center.
The location you logout will be the same location you login to the new map.

Quit your current game.

Start a new game and choose a new seed name and whatever options you like.

Click start to login for the first time.

When your character is spawned into the map, logout right away, Quit the game.

Log out and close the client game completely.

The next part may vary depending on your windows installation.

** If you feel uncomfortable doing this then don't proceed if you aren't sure what you are doing. **


Go to your AppData folder, it will look similar to this if you have a default installation:

C:\Users\YOURUSERNAME\AppData\Roaming\7DaysToDie\Saves\Random Gen\YOURPREVIOUSGAME

Replace your User Name and your Old game map name with whatever you have it set to.

Make a copy of this old game folder in case of a mistake you can revert back.
Copy and paste it in another directory as a backup.


Now, go inside this folder and you will see there is a Player folder.

Select the Player folder to highlight it, and then right click, then choose Copy.

Go back up to the folders inside Random Gen.

Go into the folder of your newly created map with the name that you chose.

Paste the Player folder into your new game folder and it should ask you to overwrite the files.
If it does not, then you are in the wrong directory.

Select Yes to merge the folder and replace the 3 files inside.


** This is optional and it won't hurt to leave it in! **

Go into the newly copied Player folder and delete the file with the extension .map
* This will erase any map data you uncovered from your previous map since it will not match anymore. *
The new map will be created when you login for the first time.


Finally!

Start the game, continue the new game and your character should be there with whatever was in your backpack plus any skills
and levels you previously attained.


If the map file wasn't deleted then you may see it doesn't match. It will correct itself as you travel
around and uncover the new map.  Or you can logout and go back and delete the .map file from the Player folder.



Please leave me any feedback, comments, suggestions, problems or bugs to me privately in discord
or send me an email listed on the Gitlab repo website.

Enjoy and Cheers!
